const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

// var id = '5b1e5b93851b4a187c4bc1f3';
//
// if (!ObjectID.isValid(id)) {
//  console.log('ID not valid');
//}

// Todo.find({
//   _id: id
// }).then((todos) => {
//   console.log('Todos:',todos);
// });
//
// Todo.findOne({
//   _id: id
// }).then((todo) => {
//   console.log('Todos:',todo);
// });

// Todo.findById(id).then((todo) => {
//   if (!todo) {
//     return console.log('Id not found');
//   }
//   console.log('Todos by Id:',todo);
// }).catch((e) => console.log(e););

User.findById('5b1c544f2c98711f8c6b088f').then((user) => {
  if (!user) {
    return console.log('unable to find the user0');
  }
  console.log(JSON.stringify(user, undefined, 2));
}, (e) => {
  console.log(e);
});
