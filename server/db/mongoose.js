var mongoose = require('mongoose');

mongoose.Promise =global.Promise;

let db = {
  localhost: 'mongodb://localhost:27017/TodoApp',
  mlab: 'mongodb://BlueCipher:imigishagusa4us@ds035836.mlab.com:35836/todos_database'
};
mongoose.connect(db.mlab || db.localhost);


module.exports = {mongoose};
